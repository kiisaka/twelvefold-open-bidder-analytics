package com.tf.openbidder.analytics.storage;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import org.junit.Before;
import org.junit.Test;

import com.google.api.services.storage.Storage;
import com.google.api.services.storage.Storage.Buckets;
import com.google.api.services.storage.Storage.Buckets.Get;
import com.google.api.services.storage.model.Bucket;
import com.google.api.services.storage.model.StorageObject;

public class GoogleStorageTest {

	GoogleStorage storage;

	final String bucketName = "viewability-logs";

	@Before
	public void setup() {
		storage = GoogleStorage.singleton;
	}

	@Test
	public void testBuckets() {

		try {
			Buckets buckets = storage.getBuckets();
			Get get = buckets.get(bucketName);
			Bucket viewabilityBucket = get.execute();
			Set<Entry<String, Object>> viewabilityBucketEntries = viewabilityBucket.entrySet();

			for (Entry<String, Object> entry : viewabilityBucketEntries) {
				System.out.println(entry.getKey() + " = " + entry.getValue());
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testBucketObjects() {

		try {
			com.google.api.services.storage.Storage.Objects.List listObjects = storage.storage.objects().list(bucketName);
			com.google.api.services.storage.model.Objects objects = listObjects.execute();
			int currentPageNumber = 0;
			while (objects.getItems() != null && !objects.getItems().isEmpty() && ++currentPageNumber <= 5) {
				for (StorageObject object : objects.getItems()) {
					System.out.println(object);

				}
				// Fetch the next page
				String nextPageToken = objects.getNextPageToken();
				if (nextPageToken == null) {
					break;
				}
				listObjects.setPageToken(nextPageToken);
				objects = listObjects.execute();
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testReadObject() {

		try {

			ByteArrayOutputStream out = new ByteArrayOutputStream();
			Storage.Objects.Get getObject = storage.storage.objects().get(bucketName,
					"viewabilitys.log.2015-09-15-22.10.216.68.224.gz");

			getObject.getMediaHttpDownloader().setDirectDownloadEnabled(false);
			getObject.executeMediaAndDownloadTo(out);

			InputStream stream = new GZIPInputStream(new ByteArrayInputStream(out.toByteArray()));
			BufferedReader input = new BufferedReader(new InputStreamReader(stream));
			while (input.ready()) {
				String line = input.readLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testStorageObjectsByDate() {
		try {
			List<StorageObject> list = storage.getViewabilityLogs(new Date());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
