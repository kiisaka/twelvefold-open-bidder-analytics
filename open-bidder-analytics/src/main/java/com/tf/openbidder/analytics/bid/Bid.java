package com.tf.openbidder.analytics.bid;

public class Bid {

	String requestId;
	String url;
	int score;
	int queryId;

	public Bid(String requestId, String url, int score, int queryId) {
		this.requestId = requestId;
		this.url = url;
		this.score = score;
		this.queryId = queryId;
	}

	public String getRequestId() {
		return requestId;
	}

	public String getUrl() {
		return url;
	}

	public int getScore() {
		return score;
	}

	public int getQueryId() {
		return queryId;
	}

	public boolean isMoreRelevantThan(Bid anotherBid) {
		return getScore() > anotherBid.getScore();
	}
}
