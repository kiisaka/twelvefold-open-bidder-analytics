package com.tf.openbidder.analytics.interfaces;

public interface Identifiable {

	public String getId();

}
