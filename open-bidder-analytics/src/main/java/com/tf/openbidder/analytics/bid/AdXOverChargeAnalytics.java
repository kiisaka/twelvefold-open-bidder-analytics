package com.tf.openbidder.analytics.bid;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Hashtable;
import java.util.zip.GZIPInputStream;

public class AdXOverChargeAnalytics {

	final static String logFilePath = "/Users/keniisaka/data";
	int queryVersions[][] = { { 18625, 28831 }, { 18638, 28830 }, { 19109, 28833 }, { 19110, 28834 }, { 18616, 28841 },
			{ 18617, 28840 }, { 18618, 28839 }, { 18619, 28838 }, { 18621, 28836 }, { 19111, 28835 }, { 19112, 28837 } };

	int counters[] = new int[queryVersions.length];
	Hashtable<String, Bid> bids = new Hashtable<>();
	Hashtable<String, Feedback> feedbacks = new Hashtable<>();

	public AdXOverChargeAnalytics(String path) throws FileNotFoundException, IOException {

		File bidFolder = new File(logFilePath + "/bids");
		File feedbackFolder = new File(logFilePath + "/feedback");

		bidFolder.isDirectory();
		File[] bidFiles = bidFolder.listFiles();
		File[] feedbackFiles = feedbackFolder.listFiles();

		readFeedbackFiles(feedbackFiles);
		readBidFiles(bidFiles);
		matchFeedbacks();

		countQueryVersions();
		System.out.println(counters);
	}

	private void matchFeedbacks() {

		for (Feedback feedback : feedbacks.values()) {
			feedback.setBid(bids.get(feedback.getId()));
		}

	}

	private void readFeedbackFiles(File[] files) throws FileNotFoundException, IOException {
		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			BufferedReader reader = getReader(file);
			readFeedbacks(reader);
		}
	}

	private void readBidFiles(File[] files) throws FileNotFoundException, IOException {
		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			BufferedReader reader = getGZIPReader(file);
			readBids(reader);
		}
	}

	private void countQueryVersions() {

		for (Feedback feedback : feedbacks.values()) {
			Bid bid = feedback.getBid();
			for (int i = 0; i < queryVersions.length; i++) {
				if (bid.getQueryId() == queryVersions[i][0]) {
					counters[i]++;
				}
			}
		}
	}

	private void readBids(BufferedReader reader) throws IOException {

		while (reader.ready()) {
			String line = reader.readLine();
			String tokens[] = line.split("\\|");

			String id = tokens[1];
			String url = tokens[2];
			int score = (int) Float.parseFloat(tokens[11]);
			int query = Integer.parseInt(tokens[16]);

			if (feedbacks.containsKey(id) && isTargetQuery(query) && (!bids.containsKey(id) || score > bids.get(id).getScore())) {
				Bid newBid = new Bid(id, url, score, query);
				bids.put(id, newBid);
			}
		}
	}

	private void readFeedbacks(BufferedReader reader) throws IOException {

		while (reader.ready()) {
			String line = reader.readLine();
			String tokens[] = line.split("\\|");

			String id = tokens[1];
			int type = Integer.parseInt(tokens[3]);
			int price = Integer.parseInt(tokens[4]);

			if (type == 1) {
				Feedback newFeedback = new Feedback(id, price);
				feedbacks.put(id, newFeedback);
			}
		}
	}

	private boolean isTargetQuery(int query) {

		for (int i = 0; i < queryVersions.length; i++) {
			if (query == queryVersions[i][0]) {
				return true;
			}
		}
		return false;
	}

	public static void main(String[] args) throws FileNotFoundException, IOException {

		new AdXOverChargeAnalytics(logFilePath);
	}

	private BufferedReader getGZIPReader(File file) throws FileNotFoundException, IOException {
		return new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(file))));
	}

	private BufferedReader getReader(File file) throws FileNotFoundException, IOException {
		return new BufferedReader(new InputStreamReader(new FileInputStream(file)));
	}

}
