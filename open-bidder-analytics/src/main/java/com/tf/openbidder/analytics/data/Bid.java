package com.tf.openbidder.analytics.data;

import java.net.MalformedURLException;
import java.net.URL;

public class Bid {

	private String id;
	private String creativeId;
	private boolean isBid;
	private URL publisherURL;
	private String exchange;
	private int height;
	private int width;
	private long minimumCPM;

	public URL getPublisherURL() {
		return publisherURL;
	}

	public void setPublisherURL(URL publisherURL) {
		this.publisherURL = publisherURL;
	}

	public String getExchange() {
		return exchange;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public long getMinimumCPM() {
		return minimumCPM;
	}

	public void setMinimumCPM(long minimumCPM) {
		this.minimumCPM = minimumCPM;
	}

	public Bid(String readLine) {

		String[] tokens = readLine.split("|");

		setId(tokens[1]);
		String urlString = tokens[2];
		try {
			if (urlString.startsWith("http"))
				this.setPublisherURL(new URL(urlString));
			else
				this.setPublisherURL(new URL("http://" + urlString));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.setExchange(tokens[4]);
		this.setHeight(Integer.parseInt(tokens[7]));
		this.setWidth(Integer.parseInt(tokens[8]));
		this.setMinimumCPM(Long.parseLong(tokens[14]));
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCreativeId() {
		return creativeId;
	}

	public void setCreativeId(String creativeId) {
		this.creativeId = creativeId;
	}

	public boolean isBid() {
		return isBid;
	}

	public void setBid(boolean isBid) {
		this.isBid = isBid;
	}

}
