package com.tf.openbidder.analytics.data;

import java.net.MalformedURLException;
import java.net.URL;

public class VideoBidRequest extends BidRequest {

	public VideoBidRequest() {
	}

	public VideoBidRequest(String readLine) {

		String[] tokens = readLine.split("|");

		setId(tokens[1]);
		String urlString = tokens[2];
		try {
			if (urlString.startsWith("http"))
				this.setPublisherURL(new URL(urlString));
			else
				this.setPublisherURL(new URL("http://" + urlString));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.setExchange(tokens[4]);
		this.setHeight(Integer.parseInt(tokens[7]));
		this.setWidth(Integer.parseInt(tokens[8]));
		this.setMinimumCPM(Long.parseLong(tokens[14]));
	}

	public VideoBidRequest(String[] tokens) {

		this.setId(tokens[1]);
		String urlString = tokens[2];
		try {
			if (urlString.startsWith("http"))
				this.setPublisherURL(new URL(urlString));
			else
				this.setPublisherURL(new URL("http://" + urlString));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.setHeight(Integer.parseInt(tokens[3]));
		this.setWidth(Integer.parseInt(tokens[4]));
		this.setMinimumCPM(Long.parseLong(tokens[6]));
		this.setPredictedViewability(Integer.parseInt(tokens[7]));
	}

	public String toString() {
		return "BidReqeust: " + getId() + " minimum price = " + getMinimumCPM() / 1000;
	}
}
