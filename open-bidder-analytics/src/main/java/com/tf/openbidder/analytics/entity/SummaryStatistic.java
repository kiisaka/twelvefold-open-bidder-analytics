package com.tf.openbidder.analytics.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EmbeddedId;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

/**
 * The persistent class for the SummaryStatistics database table.
 * 
 */
public class SummaryStatistic implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SummaryStatisticPK id;

	private double maximum;

	private double mean;

	private double minimum;

	private long n;

	private double secondMoment;

	private double standardDeviation;

	public SummaryStatistic() {
	}

	public SummaryStatistic(Date date, String keyValue, String valueType, SummaryStatistics statistics) {
		SummaryStatisticPK primaryKey = new SummaryStatisticPK();
		primaryKey.setDate(date);
		primaryKey.setKeyValue(keyValue);
		primaryKey.setValueType(valueType);
		setN(statistics.getN());
		setMinimum(statistics.getMin());
		setMaximum(statistics.getMax());
		setSecondMoment(statistics.getSecondMoment());
		setStandardDeviation(statistics.getStandardDeviation());

	}

	public SummaryStatisticPK getId() {
		return this.id;
	}

	public void setId(SummaryStatisticPK id) {
		this.id = id;
	}

	public double getMaximum() {
		return this.maximum;
	}

	public void setMaximum(double maximum) {
		this.maximum = maximum;
	}

	public double getMean() {
		return this.mean;
	}

	public void setMean(double mean) {
		this.mean = mean;
	}

	public double getMinimum() {
		return this.minimum;
	}

	public void setMinimum(double minimum) {
		this.minimum = minimum;
	}

	public long getN() {
		return this.n;
	}

	public void setN(long n) {
		this.n = n;
	}

	public double getSecondMoment() {
		return this.secondMoment;
	}

	public void setSecondMoment(double secondMoment) {
		this.secondMoment = secondMoment;
	}

	public double getStandardDeviation() {
		return this.standardDeviation;
	}

	public void setStandardDeviation(double standardDeviation) {
		this.standardDeviation = standardDeviation;
	}

}