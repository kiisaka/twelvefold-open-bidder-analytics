package com.tf.openbidder.analytics.storage;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.zip.GZIPInputStream;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.Storage.Buckets;
import com.google.api.services.storage.StorageScopes;
import com.google.api.services.storage.model.StorageObject;
import com.tf.openbidder.analytics.data.DataSet;

public class GoogleStorage {

	/** Global instance of the JSON factory. */
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static final String APPLICATION_NAME = "";
	public static final String project = "579443312789";

	public static final String viewabilityBucketName = "viewability-logs";
	public static final String bidsBucketName = "bid-logs";
	public static final String impressionsBucketName = "imp-logs";

	/** Directory to store user credentials. */
	private static final java.io.File DATA_STORE_DIR = new java.io.File(System.getProperty("user.home"), ".store");

	Storage storage;
	private NetHttpTransport httpTransport;
	private FileDataStoreFactory dataStoreFactory;

	public static final GoogleStorage singleton = new GoogleStorage();

	private Credential authorize() throws Exception {
		// load client secrets
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY,
				new InputStreamReader(DataSet.class.getResourceAsStream("/client_secrets.json")));

		// set up authorization code flow
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(httpTransport, JSON_FACTORY, clientSecrets,
				Collections.singleton(StorageScopes.DEVSTORAGE_FULL_CONTROL)).setDataStoreFactory(dataStoreFactory).build();
		// authorize
		return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
	}

	private GoogleStorage() {
		try {
			httpTransport = GoogleNetHttpTransport.newTrustedTransport();

			dataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);
			// settings
			// readSettings();
			// authorization
			Credential credential = authorize();
			// set up global Storage instance
			storage = new Storage.Builder(httpTransport, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();
			// run commands
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Buckets getBuckets() {
		return storage.buckets();
	}

	public List<StorageObject> getViewabilityLogs() throws IOException {
		return getBucketObjects(viewabilityBucketName);
	}

	public List<StorageObject> getBidLogs() throws IOException {
		return getBucketObjects(bidsBucketName);
	}

	public List<StorageObject> getImpressionLogs() throws IOException {
		return getBucketObjects(impressionsBucketName);
	}

	public List<StorageObject> getViewabilityLogs(Date date) throws IOException {
		return getBucketObjects(viewabilityBucketName, date);
	}

	public List<StorageObject> getBidLogs(Date date) throws IOException {
		return getBucketObjects(bidsBucketName, date);
	}

	public List<StorageObject> getImpressionLogs(Date date) throws IOException {
		return getBucketObjects(impressionsBucketName, date);
	}

	public List<StorageObject> getBucketObjects(String bucketName, Date date) throws IOException {

		List<StorageObject> storageObjects = getBucketObjects(bucketName);
		List<StorageObject> matchingStorageObjects = new ArrayList<StorageObject>();

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = dateFormat.format(date);

		for (StorageObject storageObject : storageObjects) {

			String name = storageObject.getName();
			if (name.contains(dateString)) {
				matchingStorageObjects.add(storageObject);
			}
		}

		return matchingStorageObjects;
	}

	public List<StorageObject> getBucketObjects(String bucketName) throws IOException {

		com.google.api.services.storage.Storage.Objects.List listObjects = storage.objects().list(bucketName);
		com.google.api.services.storage.model.Objects objects = listObjects.execute();

		List<StorageObject> list = new ArrayList<StorageObject>();

		while (objects.getItems() != null && !objects.getItems().isEmpty()) {
			for (StorageObject object : objects.getItems()) {
				list.add(object);
			}
			// Fetch the next page
			String nextPageToken = objects.getNextPageToken();
			if (nextPageToken == null) {
				break;
			}

			listObjects.setPageToken(nextPageToken);
			objects = listObjects.execute();
		}

		return list;
	}

	public BufferedReader getObjectReader(String bucketName, String objectName) throws IOException {

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Storage.Objects.Get getObject = storage.objects().get(bucketName, objectName);

		getObject.getMediaHttpDownloader().setDirectDownloadEnabled(false);
		getObject.executeMediaAndDownloadTo(out);

		return new BufferedReader(new InputStreamReader(new GZIPInputStream(new ByteArrayInputStream(out.toByteArray()))));

	}

	public BufferedReader getObjectReader(StorageObject storageObject) throws IOException {
		return getObjectReader(storageObject.getBucket(), storageObject.getName());

	}
}
