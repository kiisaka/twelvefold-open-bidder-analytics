package com.tf.openbidder.analytics.entity;

import java.io.Serializable;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The primary key class for the SummaryStatistics database table.
 * 
 */

public class SummaryStatisticPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.TIMESTAMP)
	private java.util.Date date;

	private String keyValue;

	private String valueType;

	public SummaryStatisticPK() {
	}

	public java.util.Date getDate() {
		return this.date;
	}

	public void setDate(java.util.Date date) {
		this.date = date;
	}

	public String getKeyValue() {
		return this.keyValue;
	}

	public void setKeyValue(String keyValue) {
		this.keyValue = keyValue;
	}

	public String getValueType() {
		return this.valueType;
	}

	public void setValueType(String valueType) {
		this.valueType = valueType;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof SummaryStatisticPK)) {
			return false;
		}
		SummaryStatisticPK castOther = (SummaryStatisticPK) other;
		return this.date.equals(castOther.date) && this.keyValue.equals(castOther.keyValue)
				&& this.valueType.equals(castOther.valueType);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.date.hashCode();
		hash = hash * prime + this.keyValue.hashCode();
		hash = hash * prime + this.valueType.hashCode();

		return hash;
	}
}