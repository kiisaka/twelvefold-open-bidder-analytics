package com.tf.openbidder.analytics.data;

import javax.persistence.Id;

import com.tf.openbidder.analytics.interfaces.Identifiable;

public class Impression implements Identifiable {

	@Id
	private String id;
	private int priceMicros;

	// @OneToOne(mappedBy = "id")
	private BidRequest bidRequest;
	private int creativeId;

	public Impression() {
	}

	public Impression(String readLine) {
		String[] tokens = readLine.split("\\|");
		setId(tokens[1]);
		setPriceMicros(Integer.parseInt(tokens[2]));
		setCreativeId(Integer.parseInt(tokens[7]));
	}

	public int getPriceMicros() {
		return priceMicros;
	}

	public void setPriceMicros(int priceMicros) {
		this.priceMicros = priceMicros;
	}

	public BidRequest getBidRequest() {
		return bidRequest;
	}

	public void setCreativeId(int creativeId) {
		this.creativeId = creativeId;
	}

	public int getCreativeId() {
		return creativeId;
	}

	public void setBidRequest(BidRequest bidRequest) {
		this.bidRequest = bidRequest;
		bidRequest.setImpression(this);
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String toString() {
		return "Impression: " + getId() + " price = " + getPriceMicros();
	}
}
