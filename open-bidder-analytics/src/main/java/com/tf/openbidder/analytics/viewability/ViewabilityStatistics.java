package com.tf.openbidder.analytics.viewability;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.apache.commons.math3.stat.descriptive.SynchronizedSummaryStatistics;
import org.apache.commons.math3.stat.regression.RegressionResults;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.services.storage.model.StorageObject;
import com.tf.openbidder.analytics.data.BidRequest;
import com.tf.openbidder.analytics.data.Impression;
import com.tf.openbidder.analytics.data.VideoBidRequest;
import com.tf.openbidder.analytics.storage.GoogleStorage;

public class ViewabilityStatistics {

	Date testDate;
	GoogleStorage storage = GoogleStorage.singleton;
	Hashtable<String, Impression> impressions = new Hashtable<String, Impression>();
	Hashtable<String, BidRequest> bidRequests = new Hashtable<String, BidRequest>();

	SynchronizedSummaryStatistics globalViewabilityStats = new SynchronizedSummaryStatistics();
	Hashtable<String, SynchronizedSummaryStatistics> viewabilityStatisticsByPublisher = new Hashtable<String, SynchronizedSummaryStatistics>();
	Hashtable<String, SynchronizedSummaryStatistics> minimumCPMStatisticsByPublisher = new Hashtable<String, SynchronizedSummaryStatistics>();

	int threadCount = 0;

	public static final Logger logger = LoggerFactory.getLogger(ViewabilityStatistics.class);

	public ViewabilityStatistics(Date date) {
		testDate = date;

		try {
			readData();
			calculateStatistics();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void readData() throws IOException {

		readImpressions();
		readViewability();
	}

	private void readImpressions() throws IOException {

		for (StorageObject storageObject : storage.getImpressionLogs(testDate)) {
			logger.info("Reading" + storageObject.getName());
			BufferedReader reader = storage.getObjectReader(storageObject);
			while (reader.ready()) {
				Impression impression = new Impression(reader.readLine());
				impressions.put(impression.getId(), impression);
			}
		}
	}

	private SummaryStatistics readViewability() throws IOException {

		globalViewabilityStats = new SynchronizedSummaryStatistics();

		ThreadFactory pool = Executors.defaultThreadFactory();
		for (StorageObject storageObject : storage.getViewabilityLogs(testDate)) {

			logger.info("Reading" + storageObject.getName());

			Thread newThread = pool.newThread(new ViewabilityReaderProcess(storageObject, globalViewabilityStats));
			newThread.start();
		}

		while (threadCount > 0) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return globalViewabilityStats;
	}

	private class ViewabilityReaderProcess implements Runnable {

		BufferedReader reader = null;
		SummaryStatistics stats;
		private StorageObject storageObject;

		ViewabilityReaderProcess(StorageObject storageObject, SummaryStatistics stats) {
			this.stats = stats;
			this.storageObject = storageObject;
		}

		ViewabilityReaderProcess(BufferedReader reader, SummaryStatistics stats) {
			this.stats = stats;
			this.reader = reader;
		}

		@Override
		public void run() {
			threadCount++;

			while (reader == null) {
				try {
					this.reader = storage.getObjectReader(storageObject);
				} catch (IOException e) {
					logger.error("Trying to create reader: " + e.getMessage() + ". Retrying.");
				}
			}

			try {
				while (reader.ready()) {
					String[] tokens = reader.readLine().split("\\|");
					String id = tokens[1];
					String urlString = tokens[2];
					long minimumCPM = Long.parseLong(tokens[6]);

					URL publisherURL = null;
					if (!urlString.startsWith("http")) {
						urlString = "http://" + urlString;
					}
					try {
						publisherURL = new URL(urlString);
					} catch (Exception e) {
						logger.error(e.getMessage());
					}

					String publisherHost = publisherURL.getHost();

					int viewability = Integer.parseInt(tokens[7]);

					if (viewability > 0 && publisherURL != null) {
						stats.addValue(viewability); // Threadsafe

						if (!viewabilityStatisticsByPublisher.containsKey(publisherHost)) {
							viewabilityStatisticsByPublisher.put(publisherHost, new SynchronizedSummaryStatistics());
						}
						viewabilityStatisticsByPublisher.get(publisherHost).addValue(viewability);
					}

					if (!minimumCPMStatisticsByPublisher.containsKey(publisherHost)) {
						minimumCPMStatisticsByPublisher.put(publisherHost, new SynchronizedSummaryStatistics());
					}
					minimumCPMStatisticsByPublisher.get(publisherHost).addValue(minimumCPM);

					if (impressions.containsKey(id)) {
						BidRequest bidRequest = new VideoBidRequest(tokens);
						bidRequests.put(id, bidRequest);
						impressions.get(id).setBidRequest(bidRequest);
					}
				}
			} catch (Exception e) {
				logger.error("Reading failed: " + e.getMessage());
			}
			threadCount--;
		}
	}

	private void calculateStatistics() {

		SummaryStatistics viewabilityStatistics = new SummaryStatistics();
		SummaryStatistics minimumCPMStatistics = new SummaryStatistics();
		SummaryStatistics priceStatistics = new SummaryStatistics();

		SimpleRegression viewabilityMinimumCPMRegression = new SimpleRegression();
		SimpleRegression viewabilityPriceRegression = new SimpleRegression();

		for (BidRequest bidRequest : bidRequests.values()) {

			double predictedViewability = bidRequest.getPredictedViewability();
			double minimumCPM = bidRequest.getMinimumCPM();
			double impressionPrice = bidRequest.getImpressionPrice();

			if (predictedViewability > 0) { // Eliminate cases where predicted
											// viewability < 0, ie, invalid
				viewabilityStatistics.addValue(predictedViewability);
				minimumCPMStatistics.addValue(minimumCPM / 1000);
				priceStatistics.addValue(impressionPrice);

				viewabilityMinimumCPMRegression.addData(predictedViewability, minimumCPM);
				viewabilityPriceRegression.addData(predictedViewability, impressionPrice);
			}
		}

		RegressionResults viewabilityMinimumCPMRegressionResults = viewabilityMinimumCPMRegression.regress();
		RegressionResults viewabilityPriceRegressionResults = viewabilityPriceRegression.regress();

		logger.info("Viewability");
		logger.info(viewabilityStatistics.toString());
		logger.info("Minimum CPM");
		logger.info(minimumCPMStatistics.toString());
		logger.info("Impression Price");
		logger.info(priceStatistics.toString());

		logger.info("Viewability / Minimum CPM Regression");
		logger.info("R Squared = " + viewabilityMinimumCPMRegressionResults.getRSquared());

		logger.info("Viewability / Price Regression");
		logger.info("R Squared = " + viewabilityPriceRegressionResults.getRSquared());

	}

	public static void main(String[] args) {
		Date date = new Date();
		Date yesterday = new Date(date.getTime() - 24 * 3600 * 1000);

		if (args.length == 0) {
			new ViewabilityStatistics(yesterday);
		} else {
			try {
				new ViewabilityStatistics(SimpleDateFormat.getInstance().parse(args[0]));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
