package com.tf.openbidder.analytics.bid;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.services.storage.model.StorageObject;
import com.tf.openbidder.analytics.data.Bid;
import com.tf.openbidder.analytics.storage.GoogleStorage;

public class BidStatistics {

	private Date testDate;
	GoogleStorage storage = GoogleStorage.singleton;
	private Hashtable<String, Bid> bids = new Hashtable<>();

	public static final Logger logger = LoggerFactory.getLogger(BidStatistics.class);

	public BidStatistics(Date date) {
		testDate = date;

		try {
			readData();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		calculateStatistics();
	}

	private void calculateStatistics() {
		// TODO Auto-generated method stub

	}

	private void readData() throws IOException {

		for (StorageObject storageObject : storage.getBidLogs(testDate)) {
			logger.info("Reading" + storageObject.getName());
			BufferedReader reader = storage.getObjectReader(storageObject);
			while (reader.ready()) {
				Bid bid = new Bid(reader.readLine());
				bids.put(bid.getId(), bid);
			}
		}
	}

	public static void main(String[] args) {
		Date date = new Date();
		Date yesterday = new Date(date.getTime() - 24 * 3600 * 1000);

		if (args.length == 0) {
			new BidStatistics(yesterday);
		} else {
			try {
				new BidStatistics(new SimpleDateFormat("yyyy-MM-dd").parse(args[0]));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
