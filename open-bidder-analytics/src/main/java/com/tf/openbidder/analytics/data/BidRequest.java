package com.tf.openbidder.analytics.data;

import java.net.URL;
import java.util.Date;

import javax.persistence.Id;

import com.tf.openbidder.analytics.interfaces.Identifiable;

public abstract class BidRequest implements Identifiable {

	@Id
	private String id;
	private URL publisherURL;
	private Date timestamp;
	private String exchange;
	private int predictedViewability;
	private int width, height;
	private long minimumCPM;
	private Impression impression;

	@Override
	public String getId() {
		return id;
	}

	protected void setId(String id) {
		this.id = id;
	}

	public URL getPublisherURL() {
		return publisherURL;
	}

	public void setPublisherURL(URL publisherURL) {
		this.publisherURL = publisherURL;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getExchange() {
		return exchange;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public int getPredictedViewability() {
		return predictedViewability;
	}

	public void setPredictedViewability(int predictedViewability) {
		this.predictedViewability = predictedViewability;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public long getMinimumCPM() {
		return minimumCPM;
	}

	public void setMinimumCPM(long minimumCPM) {
		this.minimumCPM = minimumCPM;
	}

	public Impression getImpression() {
		return impression;
	}

	public void setImpression(Impression impresison) {
		this.impression = impresison;
	}

	public int getImpressionPrice() {

		return getImpression() == null ? 0 : getImpression().getPriceMicros();
	}

}
