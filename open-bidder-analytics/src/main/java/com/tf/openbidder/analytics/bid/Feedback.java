package com.tf.openbidder.analytics.bid;

public class Feedback {

	String id;
	int price;
	Bid bid;

	public String getId() {
		return id;
	}

	public int getPrice() {
		return price;
	}

	public Feedback(String id, int price) {
		this.id = id;
		this.price = price;
	}

	public void setBid(Bid bid) {
		this.bid = bid;
	}

	public Bid getBid() {
		return bid;
	}
}
